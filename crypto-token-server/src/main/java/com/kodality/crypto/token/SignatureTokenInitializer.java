package com.kodality.crypto.token;

import java.security.cert.X509Certificate;
import lombok.extern.slf4j.Slf4j;
import org.digidoc4j.SignatureToken;
import org.digidoc4j.signers.PKCS11SignatureToken;

@Slf4j
public class SignatureTokenInitializer {

  private final String tokenDriver;
  private final int slot;
  private final String password;

  public SignatureTokenInitializer(String tokenDriver, int slot, String password) {
    this.tokenDriver = tokenDriver;
    this.slot = slot;
    this.password = password;
  }

  public SignatureToken initToken() {
    log.info("Initializing token, driver: {}, slot: {}", tokenDriver, slot);
    SignatureToken signatureToken = new PKCS11SignatureToken(tokenDriver, password.toCharArray(), slot);
    X509Certificate certificate = signatureToken.getCertificate();
    log.info("Certificate: {}", certificate);
    return signatureToken;
  }
}
