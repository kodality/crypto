package com.kodality.crypto.api.api;

public interface TokenEndpoints {
  String GET_CERTIFICATE = "/getCertificate";
  String SIGN = "/sign";
}
