package com.kodality.crypto.api.api;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TokenMessage {
  private String messageId;
  private String alias;
}
